package main

import (
	"fmt"
	"log"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/labstack/echo"

	//"sifiv-backend/config"
	"sifiv-backend/infrastructure/datastore"
	"sifiv-backend/infrastructure/router"
	"sifiv-backend/registry"
)

func main() {
	//config.ReadConfig()

	db := datastore.NewDB()
	db.LogMode(true)
	defer db.Close()

	r := registry.NewRegistry(db)

	e := echo.New()
	e = router.NewRouter(e, r.NewAppController())

	
	fmt.Println("Server listen at http://localhost" + ":" + "2021")
	if err := e.Start(":" + "2021"); err != nil {
		
		fmt.Print(err)
		log.Fatalln(err)
	}
	/*
	fmt.Println("Server listen at http://localhost" + ":" + config.C.Server.Address)
	if err := e.Start(":" + config.C.Server.Address); err != nil {
		log.Fatalln(err)
	}
	*/
}
