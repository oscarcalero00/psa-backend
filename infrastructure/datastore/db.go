package datastore

import (
	"fmt"
	"log"
	"sifiv-backend/config"

	//"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	//"github.com/go-sql-driver/postgres"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

func NewDB() *gorm.DB {

	config.ReadConfig()

	DBMS := "postgres"

	//host := "host=127.0.0.1 port=5432 user=postgres dbname=sifiv02 password=123456789 sslmode=disable"
	var _config config.Database
	var host string
	switch config.C.SERVERENV {
	case "test":
		_config = config.C.Database.Test
	case "develoment":
		_config = config.C.Database.Develoment
	default:
		_config = config.C.Database.Local
	}

	host = fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=disable", _config.Addr, _config.Port, _config.User, _config.DBName, _config.Password)
	fmt.Println(host)
	//host := `host=${config.C.Database.Addr} port=${config.C.Database.Port} user=${config.C.Database.UsSer} dbname=${config.C.Database.DBName} password=${config.C.Database.Password} sslmode=disable`
	//host := "host=rds-sifiv-v2.ctcmaexpcbku.us-west-2.rds.amazonaws.com port=5432 user=postgres dbname=sifiv02_develop password=fxWoYB3XIGTp9b2JeU7n sslmode=disable"
	//host = "host=127.0.0.1 port=5432 user=postgres dbname=sifiv02_202105 password=123456789 sslmode=disable"

	db, err := gorm.Open(DBMS, host)

	if err != nil {
		log.Fatalln(err)
	}

	return db
}
