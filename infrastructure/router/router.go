package router

import (
	"fmt"
	"net/http"

	//routes_auth "sifiv-backend/infrastructure/router/auth"
	routes_originacion "sifiv-backend/infrastructure/router/originacion"
	routes_public "sifiv-backend/infrastructure/router/public"
	"sifiv-backend/interface/controller"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func NewRouter(e *echo.Echo, c controller.AppController) *echo.Echo {
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept, echo.HeaderAuthorization},
		//Access - Control - Allow - Origin: []string{"*"},
	}))
	//[]string{"X-Requested-With", "Content-Type", "Authorization"})

	e.Use(func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			return next(c)
		}
	})

	e.HTTPErrorHandler = func(err error, c echo.Context) {
		if err != nil {
			e.DefaultHTTPErrorHandler(echo.NewHTTPError(http.StatusInternalServerError, err.Error()), c)
		}
		fmt.Println(err)
		//c.Logger().Error(err)
	}
	//rauth := e.Group("/api")

	r := e.Group("/api")
	//r.Use(middleware.JWT([]byte("secret")))
	//r.Use(middleware.JWT([]byte("secret")))

	//routes_auth.UseSubrouteAuth(e, c, rauth)
	//routes_auth.UseSubrouteMenu(e, c, r)
	routes_originacion.UseSubrouteBanco(e, c, r)
	routes_public.UseSubrouteSolicitud(e, c, r)

	return e
}
