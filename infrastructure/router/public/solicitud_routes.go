package public

import (
	"sifiv-backend/interface/controller"

	"github.com/labstack/echo"
)

func UseSubrouteSolicitud(e *echo.Echo, c controller.AppController, r *echo.Group) {

	r.GET("/solicitud", func(context echo.Context) error { return c.Solicitud.GetSolicituds(context) })
	r.GET("/solicitud/:id", func(context echo.Context) error { return c.Solicitud.GetSolicitud(context.Param("id"), context) })
	r.POST("/solicitud", func(context echo.Context) error { return c.Solicitud.CreateSolicitud(context) })
	r.PUT("/solicitud/:id", func(context echo.Context) error { return c.Solicitud.UpdateSolicitud(context.Param("id"), context) })
	r.DELETE("/solicitud/:id", func(context echo.Context) error { return c.Solicitud.DeleteSolicitud(context.Param("id"), context) })

}
