package originacion

import (
	"sifiv-backend/interface/controller"

	"github.com/labstack/echo"
)

func UseSubrouteBanco(e *echo.Echo, c controller.AppController, r *echo.Group) {

	r.GET("/bancos", func(context echo.Context) error { return c.Banco.GetBancos(context) })
	r.GET("/bancos/:id", func(context echo.Context) error { return c.Banco.GetBanco(context.Param("id"), context) })
	r.POST("/bancos", func(context echo.Context) error { return c.Banco.CreateBanco(context) })
	r.PUT("/bancos/:id", func(context echo.Context) error { return c.Banco.UpdateBanco(context.Param("id"), context) })
	r.DELETE("/bancos/:id", func(context echo.Context) error { return c.Banco.DeleteBanco(context.Param("id"), context) })

	r.GET("/cuentasbanco", func(context echo.Context) error { return c.CuentaBanco.GetCuentaBancos(context) })
	r.GET("/cuentasbanco/:id", func(context echo.Context) error { return c.CuentaBanco.GetCuentaBanco(context.Param("id"), context) })
	r.POST("/cuentasbanco", func(context echo.Context) error { return c.CuentaBanco.CreateCuentaBanco(context) })
	r.PUT("/cuentasbanco/:id", func(context echo.Context) error { return c.CuentaBanco.UpdateCuentaBanco(context.Param("id"), context) })
	r.DELETE("/cuentasbanco/:id", func(context echo.Context) error { return c.CuentaBanco.DeleteCuentaBanco(context.Param("id"), context) })

	
}
