package auth

import (
	model "sifiv-backend/domain/model/auth"
	presenter "sifiv-backend/usecase/presenter/auth"	
	"fmt"
)

type menuPresenter struct{}

func NewMenuPresenter() presenter.MenuPresenter {
	return &menuPresenter{}
}

func (up *menuPresenter) ResponseMenu(us []model.MenuList) []model.MenuList {	
	fmt.Println("111")
	fmt.Println(us)
	return us
}

