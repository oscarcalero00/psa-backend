package auth

import (
	model "sifiv-backend/domain/model/auth"
	presenter "sifiv-backend/usecase/presenter/auth"	
	//"fmt"
)

type loginPresenter struct{}

func NewLoginPresenter() presenter.LoginPresenter {
	return &loginPresenter{}
}

func (up *loginPresenter) ResponseLogin(us *model.LoginReponse) *model.LoginReponse {	
	return us
}

