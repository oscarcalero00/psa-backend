package public

import (
	"errors"
	//"fmt"
	"net/http"
	model "sifiv-backend/domain/model/public"
	interactor "sifiv-backend/usecase/interactor/public"
	//	"strconv"
	//"github.com/labstack/echo"
)

type solicitudController struct {
	solicitudInteractor interactor.SolicitudInteractor
}

type SolicitudController interface {
	GetSolicituds(c Context) error
	CreateSolicitud(c Context) error
	GetSolicitud(id string, c Context) error
	UpdateSolicitud(id string, c Context) error
	DeleteSolicitud(id string, c Context) error
}

func NewSolicitudController(us interactor.SolicitudInteractor) SolicitudController {
	return &solicitudController{us}
}

func (uc *solicitudController) GetSolicituds(c Context) error {
	//fmt.Println("121121221")
	var u []*model.SolicitudResponse

	var params model.SolicitudQuery
	if err := c.Bind(&params); !errors.Is(err, nil) {
		return err
	}

	u, err := uc.solicitudInteractor.Get(u, params)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, u)
}

func (uc *solicitudController) CreateSolicitud(c Context) error {

	var params model.Solicitud

	if err := c.Bind(&params); !errors.Is(err, nil) {
		return err
	}

	u, err := uc.solicitudInteractor.Create(&params)

	if !errors.Is(err, nil) {
		return err
	}
	return c.JSON(http.StatusCreated, u)
}

func (uc *solicitudController) GetSolicitud(id string, c Context) error {

	var params model.SolicitudResponse

	if err := c.Bind(&params); !errors.Is(err, nil) {
		return err
	}

	//id := "CO"

	u, err := uc.solicitudInteractor.GetRecord(id, &params)

	if !errors.Is(err, nil) {
		return err
	}
	return c.JSON(http.StatusCreated, u)
}

func (uc *solicitudController) UpdateSolicitud(id string, c Context) error {

	var params model.Solicitud

	if err := c.Bind(&params); !errors.Is(err, nil) {
		return err
	}

	//id := "CO"

	u, err := uc.solicitudInteractor.Update(id, &params)

	if !errors.Is(err, nil) {
		return err
	}
	return c.JSON(http.StatusCreated, u)
}

func (uc *solicitudController) DeleteSolicitud(id string, c Context) error {

	var params model.Solicitud

	if err := c.Bind(&params); !errors.Is(err, nil) {
		return err
	}

	u, err := uc.solicitudInteractor.Delete(id, &params)

	if !errors.Is(err, nil) {
		return err
	}

	return c.JSON(http.StatusCreated, u)
}
