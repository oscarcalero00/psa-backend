package public

type Context interface {
	JSON(code int, i interface{}) error
	Bind(i interface{}) error
	Get(i string) interface{}
}
