package auth

import (
	"errors"
	//"fmt"
	model "sifiv-backend/domain/model/auth"
	interactor "sifiv-backend/usecase/interactor/auth"
	"net/http"	
)

type loginController struct {
	loginInteractor interactor.LoginInteractor
}

type LoginController interface {
	Login(c Context) error	
}

func NewLoginController(us interactor.LoginInteractor) LoginController {
	return &loginController{us}
}
/*
func (uc *loginController) Login(c Context) error {
	var u *model.Login

	u, err := uc.loginInteractor.Login(u)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, u)
}*/

func (uc *loginController) Login(c Context) error {
	


	var params model.Login

	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}
	
	u, err := uc.loginInteractor.Login(&params)	

	if !errors.Is(err, nil) {			
		return err
	}	
	
	if u == nil {
		return c.JSON(http.StatusUnauthorized, "Usuario Invalido")	
	}
	return c.JSON(http.StatusCreated, u)
}


