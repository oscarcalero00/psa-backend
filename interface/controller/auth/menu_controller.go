package auth

import (
	"errors"
	"fmt"
	model "sifiv-backend/domain/model/auth"
	interactor "sifiv-backend/usecase/interactor/auth"
	"net/http"	
)

type menuController struct {
	menuInteractor interactor.MenuInteractor
}

type MenuController interface {
	Menu(c Context) error	
}

func NewMenuController(us interactor.MenuInteractor) MenuController {
	return &menuController{us}
}
/*
func (uc *menuController) Menu(c Context) error {
	var u *model.Menu

	u, err := uc.menuInteractor.Menu(u)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, u)
}*/

func (uc *menuController) Menu(c Context) error {
	
	//fmt.Println("11111111")

	var params model.Menu

	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}
	//fmt.Println("22222222")
	u, err := uc.menuInteractor.Menu(&params)	

	if !errors.Is(err, nil) {			
		return err
	}	
	//fmt.Println("33333333")
	if u == nil {
		//return c.JSON(http.StatusUnauthorized, "Usuario Invalido")	
	}
	fmt.Println(u)
	return c.JSON(http.StatusCreated, u)
}


