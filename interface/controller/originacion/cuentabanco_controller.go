package originacion

import (
	"errors"
	//"fmt"
	model "sifiv-backend/domain/model/originacion"
	interactor "sifiv-backend/usecase/interactor/originacion"
	"net/http"
//	"strconv"

	//"github.com/labstack/echo"
)

type cuentaBancoController struct {
	cuentaBancoInteractor interactor.CuentaBancoInteractor
}

type CuentaBancoController interface {
	GetCuentaBancos(c Context) error
	CreateCuentaBanco(c Context) error
	GetCuentaBanco(id string,c Context) error
	UpdateCuentaBanco(id string,c Context) error
	DeleteCuentaBanco(id string,c Context) error
}

func NewCuentaBancoController(us interactor.CuentaBancoInteractor) CuentaBancoController {
	return &cuentaBancoController{us}
}

func (uc *cuentaBancoController) GetCuentaBancos(c Context) error {
	//fmt.Println("121121221")
	var u []*model.CuentaBancoResponse


	var params model.CuentaBancoQuery
	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}

	u, err := uc.cuentaBancoInteractor.Get(u,params)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, u)
}

func (uc *cuentaBancoController) CreateCuentaBanco(c Context) error {
	
	var params model.CuentaBanco

	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}
	
	u, err := uc.cuentaBancoInteractor.Create(&params)	

	if !errors.Is(err, nil) {			
		return err
	}		
	return c.JSON(http.StatusCreated, u)
}

func (uc *cuentaBancoController) GetCuentaBanco(id string,c Context) error {
	
	var params model.CuentaBancoResponse

	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}

	//id := "CO"
	
	u, err := uc.cuentaBancoInteractor.GetRecord(id,&params)	

	if !errors.Is(err, nil) {			
		return err
	}		
	return c.JSON(http.StatusCreated, u)
}

func (uc *cuentaBancoController) UpdateCuentaBanco(id string,c Context) error {
	
	var params model.CuentaBanco

	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}

	//id := "CO"
	
	u, err := uc.cuentaBancoInteractor.Update(id,&params)	

	if !errors.Is(err, nil) {			
		return err
	}		
	return c.JSON(http.StatusCreated, u)
}

func (uc *cuentaBancoController) DeleteCuentaBanco(id string,c Context) error {
	
	var params model.CuentaBanco
	
	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}
	
	u, err := uc.cuentaBancoInteractor.Delete(id,&params)	

	if !errors.Is(err, nil) {			
		return err
	}	
	

	return c.JSON(http.StatusCreated, u)
}

