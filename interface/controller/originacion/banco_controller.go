package originacion

import (
	"errors"
	//"fmt"
	model "sifiv-backend/domain/model/originacion"
	interactor "sifiv-backend/usecase/interactor/originacion"
	"net/http"
//	"strconv"

	//"github.com/labstack/echo"
)

type bancoController struct {
	bancoInteractor interactor.BancoInteractor
}

type BancoController interface {
	GetBancos(c Context) error
	CreateBanco(c Context) error
	GetBanco(id string,c Context) error
	UpdateBanco(id string,c Context) error
	DeleteBanco(id string,c Context) error
}

func NewBancoController(us interactor.BancoInteractor) BancoController {
	return &bancoController{us}
}

func (uc *bancoController) GetBancos(c Context) error {
	//fmt.Println("121121221")
	var u []*model.BancoResponse


	var params model.BancoQuery
	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}

	u, err := uc.bancoInteractor.Get(u,params)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, u)
}

func (uc *bancoController) CreateBanco(c Context) error {
	
	var params model.Banco

	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}
	
	u, err := uc.bancoInteractor.Create(&params)	

	if !errors.Is(err, nil) {			
		return err
	}		
	return c.JSON(http.StatusCreated, u)
}

func (uc *bancoController) GetBanco(id string,c Context) error {
	
	var params model.BancoResponse

	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}

	//id := "CO"
	
	u, err := uc.bancoInteractor.GetRecord(id,&params)	

	if !errors.Is(err, nil) {			
		return err
	}		
	return c.JSON(http.StatusCreated, u)
}

func (uc *bancoController) UpdateBanco(id string,c Context) error {
	
	var params model.Banco

	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}

	//id := "CO"
	
	u, err := uc.bancoInteractor.Update(id,&params)	

	if !errors.Is(err, nil) {			
		return err
	}		
	return c.JSON(http.StatusCreated, u)
}

func (uc *bancoController) DeleteBanco(id string,c Context) error {
	
	var params model.Banco
	
	if err := c.Bind(&params); !errors.Is(err, nil) {		
		return err
	}
	
	u, err := uc.bancoInteractor.Delete(id,&params)	

	if !errors.Is(err, nil) {			
		return err
	}	
	

	return c.JSON(http.StatusCreated, u)
}

