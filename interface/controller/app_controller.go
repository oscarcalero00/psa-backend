package controller

import (
	controllerAuth "sifiv-backend/interface/controller/auth"
	controllerOriginacion "sifiv-backend/interface/controller/originacion"
	controllerPublic "sifiv-backend/interface/controller/public"
)

type AppController struct {
	Login interface{ controllerAuth.LoginController }
	Menu  interface{ controllerAuth.MenuController }

	Banco interface {
		controllerOriginacion.BancoController
	}
	CuentaBanco interface {
		controllerOriginacion.CuentaBancoController
	}
	Solicitud interface {
		controllerPublic.SolicitudController
	}
}
