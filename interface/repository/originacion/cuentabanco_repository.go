package originacion

import (
	"errors"
	model "sifiv-backend/domain/model/originacion"
	repository "sifiv-backend/usecase/repository/originacion"
	"strings"
	//"fmt"
	"github.com/jinzhu/gorm"
)

type cuentaBancoRepository struct {
	db *gorm.DB
}

func NewCuentaBancoRepository(db *gorm.DB) repository.CuentaBancoRepository {
	return &cuentaBancoRepository{db}
}

type ResultCuentaBanco struct {

	ID string

}

func (ur *cuentaBancoRepository) FindAll(u []*model.CuentaBancoResponse,params model.CuentaBancoQuery) ([]*model.CuentaBancoResponse, error) {
	//var resp =  []*model.CuentaBanco{}
	var fields strings.Builder
	fields.WriteString("cuenta_banco.*")	
	fields.WriteString(",tercero_nombrecompleto as empresa_nombre")		
	fields.WriteString(",tasabase_nombre")		
	fields.WriteString(",tipotasa_nombre")		
	fields.WriteString(",tipoprba_nombre")		
	//fields.WriteString(",banco_nombre")		

	var query = ur.db.
	Select(fields.String()).		
	Joins("JOIN public.banco             ON ( banco_codigo = cuenbanc_banco  )").	
	Joins("JOIN public.empresa           ON ( empresa_codigo = cuenbanc_empresa  )").	
	Joins("JOIN public.tercero           ON ( empresa_numeroidentificacion = tercero_numeroidentificacion  )").	
	Joins("JOIN public.tercero_direccion ON ( tercdire_numeroidentificacion = empresa_numeroidentificacion AND tercdire_codigodireccion = empresa_codigodireccion  )").	
	Joins("JOIN public.tasa_base         ON ( tasabase_codigo = cuenbanc_tasabase  )").	
	Joins("JOIN public.tipo_prodbanc     ON ( tipoprba_codigo = cuenbanc_tipoproductobanco  )").	
	Joins("JOIN public.tipo_tasa         ON ( tipotasa_codigo = cuenbanc_tipotasa  )").	
	Order("cuenbanc_empresa,cuenbanc_numerocuenta")

	if(params.Cuenbanc_banco != ""){
		query = query.Where("cuenbanc_banco = ?", params.Cuenbanc_banco)
	}

	


	err := query.Find(&u).	
	Error
	
	//err := ur.db.Find(&u).Error
	if err != nil {		
		return nil, err
	}

	return u, nil




}

func (ur *cuentaBancoRepository) FindOne(id string,u *model.CuentaBancoResponse) (*model.CuentaBancoResponse, error) {
	

	var fields strings.Builder
	fields.WriteString("cuenta_banco.*")	
	fields.WriteString(",tercero_nombrecompleto as empresa_nombre")		
	fields.WriteString(",tasabase_nombre")		
	fields.WriteString(",tipotasa_nombre")		
	fields.WriteString(",tipoprba_nombre")		
	//fields.WriteString(",banco_nombre")		

	var query = ur.db.
	Select(fields.String()).		
	Joins("JOIN public.banco             ON ( banco_codigo = cuenbanc_banco  )").	
	Joins("JOIN public.empresa           ON ( empresa_codigo = cuenbanc_empresa  )").	
	Joins("JOIN public.tercero           ON ( empresa_numeroidentificacion = tercero_numeroidentificacion  )").	
	Joins("JOIN public.tercero_direccion ON ( tercdire_numeroidentificacion = empresa_numeroidentificacion AND tercdire_codigodireccion = empresa_codigodireccion  )").	
	Joins("JOIN public.tasa_base         ON ( tasabase_codigo = cuenbanc_tasabase  )").	
	Joins("JOIN public.tipo_prodbanc     ON ( tipoprba_codigo = cuenbanc_tipoproductobanco  )").	
	Joins("JOIN public.tipo_tasa         ON ( tipotasa_codigo = cuenbanc_tipotasa  )").	
	Order("cuenbanc_empresa,cuenbanc_numerocuenta")

	

	err := query.Model(u).First(&u, []string{id} ).
	Error	
	
	//err := ur.db.Find(&u).Error
	if err != nil {		
		return nil, err
	}
/*
	err := ur.db.Model(u).First(&u, []string{id} ).Error
	if err != nil {		
		return nil, err
	}*/

	return u, nil
}

// func (ur *cuentaBancoRepository) Create2(u *model.CuentaBanco) (*model.CuentaBanco, error) {
	
// 	if err := ur.db.Create(u).Error; !errors.Is(err, nil) {
// 		return nil, err
// 	}

// 	return u, nil
// }


func (ur *cuentaBancoRepository) Create( u *model.CuentaBanco) (*model.CuentaBancoResponse, error) {	
	

	u.Cuenbanc_fregistro="now()"
	u.Cuenbanc_festado="NOW()"
	u.Cuenbanc_estado="AC"
	if err := ur.db.Create(u).Error; !errors.Is(err, nil) {
		return nil, err
	}

	//return u, nil

	var actividad *model.CuentaBancoResponse = &model.CuentaBancoResponse{}
	
	//actividad, err3 := ur.FindOne(result.ID,actividad)
	//if err3 != nil {		
	//	return nil, err3
	//}
	
	return actividad, nil

	//return u, nil
}

func (ur *cuentaBancoRepository) Update(id string, u *model.CuentaBanco) (*model.CuentaBancoResponse, error) {
	u.Cuenbanc_id = id
	if err := ur.db.Model(u).Updates(u).Error; !errors.Is(err, nil) {
		return nil, err
	}

	var actividad *model.CuentaBancoResponse = &model.CuentaBancoResponse{}
	
	
	
	return actividad, nil
}

func (ur *cuentaBancoRepository) Delete(id string, u *model.CuentaBanco) (*model.CuentaBanco, error) {	
	
	
		
	
	if err := ur.db.Delete(u,[]string{id}).Error; !errors.Is(err, nil) {
		return nil, err
	}

	return u, nil
}
