package originacion

import (
	"errors"
	model "sifiv-backend/domain/model/originacion"
	repository "sifiv-backend/usecase/repository/originacion"
	"strings"
	//"fmt"
	"github.com/jinzhu/gorm"
)

type bancoRepository struct {
	db *gorm.DB
}

func NewBancoRepository(db *gorm.DB) repository.BancoRepository {
	return &bancoRepository{db}
}

type ResultBanco struct {

	ID string

}

func (ur *bancoRepository) FindAll(u []*model.BancoResponse,params model.BancoQuery) ([]*model.BancoResponse, error) {
	//var resp =  []*model.Banco{}
	var fields strings.Builder
	fields.WriteString("banco.*")	
	fields.WriteString(",TBA.tercero_apellido1          AS banco_apellido1")		
	fields.WriteString(",TBA.tercero_apellido2          AS banco_apellido2")		
	fields.WriteString(",TBA.tercero_nombre1            AS banco_nombre1")		
	fields.WriteString(",TBA.tercero_nombre2            AS banco_nombre2")		
	fields.WriteString(",TBA.tercero_nombrecompleto     AS banco_nombrecompleto")		
	fields.WriteString(",TBA.tercero_tipopersona        AS banco_tipopersona")	
	fields.WriteString(",TBA.tercero_tipoidentificacion AS banco_tipoidentificacion")		
	fields.WriteString(",TDBA.tercdire_departamento     AS banco_departamento")		
	fields.WriteString(",TDBA.tercdire_ciudad           AS banco_ciudad")		
	fields.WriteString(",TDBA.tercdire_direccion        AS banco_direccion")		
	fields.WriteString(",TDBA.tercdire_telefono         AS banco_telefono")	
	fields.WriteString(",TDBA.tercdire_celular          AS banco_celular")	
	fields.WriteString(",TDBA.tercdire_email            AS banco_email")	
	fields.WriteString(",CBA.ciudad_nombre              AS banco_ciudadnombre")	

	
	var query = ur.db.
	Select(fields.String()).		
	Joins("JOIN public.tercero           TBA  ON ( TBA.tercero_numeroidentificacion = banco_numeroidentificacion   )").	
	Joins("JOIN public.tercero_direccion TDBA ON ( TDBA.tercdire_numeroidentificacion = banco_numeroidentificacion AND TDBA.tercdire_codigodireccion = banco_codigodireccion  )").	
	Joins("JOIN public.ciudad            CBA  ON ( TDBA.tercdire_departamento = CBA.ciudad_departamento AND TDBA.tercdire_ciudad = CBA.ciudad_codigo  )").		
	Order("banco_nombrecompleto")


	if(params.Banco_codigo != ""){
		query = query.Where("banco_codigo = ?", params.Banco_codigo)
	}

	if(params.Banco_nombrecompleto != ""){
		query = query.Where("TBA.tercero_nombrecompleto ilike ?",  "%"+params.Banco_nombrecompleto+"%")
	}

	if(params.Banco_numeroidentificacion != ""){
		query = query.Where("banco_numeroidentificacion::VARCHAR ilike ?",  "%"+params.Banco_numeroidentificacion+"%")
	}


	err := query.Find(&u).	
	Error
	
	//err := ur.db.Find(&u).Error
	if err != nil {		
		return nil, err
	}

	return u, nil




}

func (ur *bancoRepository) FindOne(id string,u *model.BancoResponse) (*model.BancoResponse, error) {
	

	var fields strings.Builder
	fields.WriteString("banco.*")	
	fields.WriteString(",TBA.tercero_apellido1          AS banco_apellido1")		
	fields.WriteString(",TBA.tercero_apellido2          AS banco_apellido2")		
	fields.WriteString(",TBA.tercero_nombre1            AS banco_nombre1")		
	fields.WriteString(",TBA.tercero_nombre2            AS banco_nombre2")		
	fields.WriteString(",TBA.tercero_nombrecompleto     AS banco_nombrecompleto")		
	fields.WriteString(",TBA.tercero_tipopersona        AS banco_tipopersona")	
	fields.WriteString(",TBA.tercero_tipoidentificacion AS banco_tipoidentificacion")		
	fields.WriteString(",TDBA.tercdire_departamento     AS banco_departamento")		
	fields.WriteString(",TDBA.tercdire_ciudad           AS banco_ciudad")		
	fields.WriteString(",TDBA.tercdire_direccion        AS banco_direccion")		
	fields.WriteString(",TDBA.tercdire_telefono         AS banco_telefono")	
	fields.WriteString(",TDBA.tercdire_celular          AS banco_celular")	
	fields.WriteString(",TDBA.tercdire_email            AS banco_email")	
	fields.WriteString(",CBA.ciudad_nombre              AS banco_ciudadnombre")	

	
	var query = ur.db.
	Select(fields.String()).		
	Joins("JOIN public.tercero           TBA  ON ( TBA.tercero_numeroidentificacion = banco_numeroidentificacion   )").	
	Joins("JOIN public.tercero_direccion TDBA ON ( TDBA.tercdire_numeroidentificacion = banco_numeroidentificacion AND TDBA.tercdire_codigodireccion = banco_codigodireccion  )").	
	Joins("JOIN public.ciudad            CBA  ON ( TDBA.tercdire_departamento = CBA.ciudad_departamento AND TDBA.tercdire_ciudad = CBA.ciudad_codigo  )").		
	Order("banco_nombrecompleto")
	//Find(&u).
	

	err := query.Model(u).First(&u, []string{id} ).
	Error	
	
	//err := ur.db.Find(&u).Error
	if err != nil {		
		return nil, err
	}
/*
	err := ur.db.Model(u).First(&u, []string{id} ).Error
	if err != nil {		
		return nil, err
	}*/

	return u, nil
}

// func (ur *bancoRepository) Create2(u *model.Banco) (*model.Banco, error) {
	
// 	if err := ur.db.Create(u).Error; !errors.Is(err, nil) {
// 		return nil, err
// 	}

// 	return u, nil
// }


func (ur *bancoRepository) Create( u *model.Banco) (*model.BancoResponse, error) {	
	
	if(ur.db.AutoMigrate().HasTable("tmp_banco_operacion")){		
		ur.db.AutoMigrate().DropTable(&model.Banco{})
	}
	ur.db.AutoMigrate().CreateTable().CreateTable(&model.Banco{}) //.Migrator().CreateTable(&User{})

	u.Banco_fregistro =  `NOW()`
	u.Banco_festado   =  `NOW()`
	u.Banco_estado    =  `AC`
	
	if err := ur.db.Create(u).Error; !errors.Is(err, nil) {
		return nil, err
	}

	
	
	var result ResultBanco
	err2 :=  ur.db.Raw("select stpSIFIV_banco_crud( null,'INSERT',null) as ID").Scan(&result).Error
	if err2 != nil {		
		return nil, err2
	}
		
	

	if(ur.db.AutoMigrate().HasTable("tmp_banco_operacion")){
		ur.db.AutoMigrate().DropTable(&model.Banco{})
	}

	var actividad *model.BancoResponse = &model.BancoResponse{}
	
	actividad, err3 := ur.FindOne(result.ID,actividad)
	if err3 != nil {		
		return nil, err3
	}
	
	return actividad, nil

	//return u, nil
}

func (ur *bancoRepository) Update(id string, u *model.Banco) (*model.BancoResponse, error) {
	
	if(ur.db.AutoMigrate().HasTable("tmp_banco_operacion")){		
		ur.db.AutoMigrate().DropTable(&model.Banco{})
	}
	ur.db.AutoMigrate().CreateTable().CreateTable(&model.Banco{}) //.Migrator().CreateTable(&User{})

	u.Banco_festado   =  `NOW()`
	
	if err := ur.db.Create(u).Error; !errors.Is(err, nil) {
		return nil, err
	}	
	
	var result ResultBanco
	err2 :=  ur.db.Raw("select stpSIFIV_banco_crud( null,'UPDATE',?) as ID",id).Scan(&result).Error
	if err2 != nil {		
		return nil, err2
	}

	if(ur.db.AutoMigrate().HasTable("tmp_banco_operacion")){
		ur.db.AutoMigrate().DropTable(&model.Banco{})
	}

	var actividad *model.BancoResponse = &model.BancoResponse{}
	
	actividad, err3 := ur.FindOne(result.ID,actividad)
	if err3 != nil {		
		return nil, err3
	}
	
	return actividad, nil
}

func (ur *bancoRepository) Delete(id string, u *model.Banco) (*model.Banco, error) {	
	
	var result ResultBanco
	err2 :=  ur.db.Raw("select stpSIFIV_banco_crud( null,'DELETE',?) as ID",id).Scan(&result).Error
	if err2 != nil {		
		return nil, err2
	}

	return u, nil
}
