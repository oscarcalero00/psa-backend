package auth

import (
	"errors"
	//"fmt"
	model "sifiv-backend/domain/model/auth"
	repository "sifiv-backend/usecase/repository/auth"
	"github.com/jinzhu/gorm"
)

type loginRepository struct {
	db *gorm.DB
}

func NewLoginRepository(db *gorm.DB) repository.LoginRepository {
	return &loginRepository{db}
}

func (ur *loginRepository) Login(mo *model.Login) (*model.LoginReponse, error) {
		var resp = model.LoginReponse{}
	err := ur.db.Where("usuario_codigo = ? AND usuario_password = ? ", mo.Email,mo.Password).Select("usuario.*, tercero_direccion.*").Joins("join public.tercero_direccion on ( tercero_direccion.tercdire_numeroidentificacion = usuario.usuario_numeroidentificacion AND tercero_direccion.tercdire_codigodireccion = usuario.usuario_codigodireccion  )").First(&resp).Error

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil
	}

	if err != nil {		
		return nil, err
	}

	return &resp, nil
}

