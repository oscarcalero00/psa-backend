package auth

import (
	"errors"
	//"fmt"
	model "sifiv-backend/domain/model/auth"
	repository "sifiv-backend/usecase/repository/auth"

	"github.com/jinzhu/gorm"
)

type menuRepository struct {
	db *gorm.DB
}

func NewMenuRepository(db *gorm.DB) repository.MenuRepository {
	return &menuRepository{db}
}

func (ur *menuRepository) Menu(mo *model.Menu) ([]*model.MenuReponse, error) {
	var resp = []*model.MenuReponse{}
	err := ur.db.Select("programa_modulo.*").
		//Joins("join public.tercero_direccion on ( tercero_direccion.tercdire_numeroidentificacion = usuario.usuario_numeroidentificacion AND tercero_direccion.tercdire_codigodireccion = usuario.usuario_codigodireccion  )").
		//First(&resp).
		Where("progmodu_estado = ?", "AC").
		Order("progmodu_codigomenu asc").
		Find(&resp).
		Error

	if errors.Is(err, gorm.ErrRecordNotFound) {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return resp, nil
}
