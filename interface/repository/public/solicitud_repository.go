package public

import (
	"errors"
	model "sifiv-backend/domain/model/public"
	repository "sifiv-backend/usecase/repository/public"
	"strings"

	//"fmt"
	"github.com/jinzhu/gorm"
)

type solicitudRepository struct {
	db *gorm.DB
}

func NewSolicitudRepository(db *gorm.DB) repository.SolicitudRepository {
	return &solicitudRepository{db}
}

type ResultSolicitud struct {
	ID string
}

func (ur *solicitudRepository) FindAll(u []*model.SolicitudResponse, params model.SolicitudQuery) ([]*model.SolicitudResponse, error) {
	//var resp =  []*model.Solicitud{}
	var fields strings.Builder
	fields.WriteString("solicitudes.*")

	var query = ur.db.
		Select(fields.String()).
		//Joins("JOIN public.banco             ON ( banco_codigo = cuenbanc_banco  )").
		Order("solicitud_nombre")

	if params.Solicitud_cedula != "" {
		query = query.Where("solicitud_cedula = ?", params.Solicitud_cedula)
	}

	err := query.Find(&u).
		Error

	//err := ur.db.Find(&u).Error
	if err != nil {
		return nil, err
	}

	return u, nil

}

func (ur *solicitudRepository) FindOne(id string, u *model.SolicitudResponse) (*model.SolicitudResponse, error) {

	var fields strings.Builder
	fields.WriteString("solicitudes.*")

	var query = ur.db.
		Select(fields.String()).
		//Joins("JOIN public.banco             ON ( banco_codigo = cuenbanc_banco  )").
		Order("solicitud_cedula")

	err := query.Model(u).First(&u, []string{id}).
		Error

	//err := ur.db.Find(&u).Error
	if err != nil {
		return nil, err
	}
	/*
		err := ur.db.Model(u).First(&u, []string{id} ).Error
		if err != nil {
			return nil, err
		}*/

	return u, nil
}

// func (ur *solicitudRepository) Create2(u *model.Solicitud) (*model.Solicitud, error) {

// 	if err := ur.db.Create(u).Error; !errors.Is(err, nil) {
// 		return nil, err
// 	}

// 	return u, nil
// }

func (ur *solicitudRepository) Create(u *model.Solicitud) (*model.SolicitudResponse, error) {

	if err := ur.db.Create(u).Error; !errors.Is(err, nil) {
		return nil, err
	}

	//return u, nil

	var actividad *model.SolicitudResponse = &model.SolicitudResponse{}

	//actividad, err3 := ur.FindOne(result.ID,actividad)
	//if err3 != nil {
	//	return nil, err3
	//}

	return actividad, nil

	//return u, nil
}

func (ur *solicitudRepository) Update(id string, u *model.Solicitud) (*model.SolicitudResponse, error) {
	u.Id = id
	if err := ur.db.Model(u).Updates(u).Error; !errors.Is(err, nil) {
		return nil, err
	}

	var actividad *model.SolicitudResponse = &model.SolicitudResponse{}

	return actividad, nil
}

func (ur *solicitudRepository) Delete(id string, u *model.Solicitud) (*model.Solicitud, error) {

	if err := ur.db.Delete(u, []string{id}).Error; !errors.Is(err, nil) {
		return nil, err
	}

	return u, nil
}
