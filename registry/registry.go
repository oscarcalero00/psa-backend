package registry

import (
	"sifiv-backend/interface/controller"

	"github.com/jinzhu/gorm"
)

type registry struct {
	db *gorm.DB
}

type Registry interface {
	NewAppController() controller.AppController
}

func NewRegistry(db *gorm.DB) Registry {
	return &registry{db}
}

func (r *registry) NewAppController() controller.AppController {
	return controller.AppController{
		Solicitud:   r.NewSolicitudController(),
		Banco:       r.NewBancoController(),
		CuentaBanco: r.NewCuentaBancoController(),
	}
}
