package registry

import (
	controller "sifiv-backend/interface/controller/originacion"
	ip "sifiv-backend/interface/presenter/originacion"
	irbase "sifiv-backend/interface/repository"
	ir "sifiv-backend/interface/repository/originacion"
	interactor "sifiv-backend/usecase/interactor/originacion"
	up "sifiv-backend/usecase/presenter/originacion"
	ur "sifiv-backend/usecase/repository/originacion"
)



func (r *registry) NewCuentaBancoController() controller.CuentaBancoController {
	return controller.NewCuentaBancoController(r.NewCuentaBancoInteractor())
}

func (r *registry) NewCuentaBancoInteractor() interactor.CuentaBancoInteractor {
	return interactor.NewCuentaBancoInteractor(r.NewCuentaBancoRepository(), r.NewCuentaBancoPresenter(), irbase.NewDBRepository(r.db))
}

func (r *registry) NewCuentaBancoRepository() ur.CuentaBancoRepository {
	return ir.NewCuentaBancoRepository(r.db)
}

func (r *registry) NewCuentaBancoPresenter() up.CuentaBancoPresenter {
	return ip.NewCuentaBancoPresenter()
}

