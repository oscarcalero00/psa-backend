package registry

import (
	controller "sifiv-backend/interface/controller/originacion"
	ip "sifiv-backend/interface/presenter/originacion"
	irbase "sifiv-backend/interface/repository"
	ir "sifiv-backend/interface/repository/originacion"
	interactor "sifiv-backend/usecase/interactor/originacion"
	up "sifiv-backend/usecase/presenter/originacion"
	ur "sifiv-backend/usecase/repository/originacion"
)



func (r *registry) NewBancoController() controller.BancoController {
	return controller.NewBancoController(r.NewBancoInteractor())
}

func (r *registry) NewBancoInteractor() interactor.BancoInteractor {
	return interactor.NewBancoInteractor(r.NewBancoRepository(), r.NewBancoPresenter(), irbase.NewDBRepository(r.db))
}

func (r *registry) NewBancoRepository() ur.BancoRepository {
	return ir.NewBancoRepository(r.db)
}

func (r *registry) NewBancoPresenter() up.BancoPresenter {
	return ip.NewBancoPresenter()
}

