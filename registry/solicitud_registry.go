package registry

import (
	controller "sifiv-backend/interface/controller/public"
	ip "sifiv-backend/interface/presenter/public"
	irbase "sifiv-backend/interface/repository"
	ir "sifiv-backend/interface/repository/public"
	interactor "sifiv-backend/usecase/interactor/public"
	up "sifiv-backend/usecase/presenter/public"
	ur "sifiv-backend/usecase/repository/public"
)

func (r *registry) NewSolicitudController() controller.SolicitudController {
	return controller.NewSolicitudController(r.NewSolicitudInteractor())
}

func (r *registry) NewSolicitudInteractor() interactor.SolicitudInteractor {
	return interactor.NewSolicitudInteractor(r.NewSolicitudRepository(), r.NewSolicitudPresenter(), irbase.NewDBRepository(r.db))
}

func (r *registry) NewSolicitudRepository() ur.SolicitudRepository {
	return ir.NewSolicitudRepository(r.db)
}

func (r *registry) NewSolicitudPresenter() up.SolicitudPresenter {
	return ip.NewSolicitudPresenter()
}
