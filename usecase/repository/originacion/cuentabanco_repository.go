package originacion

import model "sifiv-backend/domain/model/originacion"

type CuentaBancoRepository interface {	
	FindAll(u []*model.CuentaBancoResponse,params model.CuentaBancoQuery) ([]*model.CuentaBancoResponse, error)
	FindOne(id string, u *model.CuentaBancoResponse) (*model.CuentaBancoResponse, error)
	Create(u *model.CuentaBanco) (*model.CuentaBancoResponse, error)	
	Update(id string, u *model.CuentaBanco) (*model.CuentaBancoResponse, error)
	Delete(id string, u *model.CuentaBanco) (*model.CuentaBanco, error)
}