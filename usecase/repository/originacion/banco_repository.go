package originacion

import model "sifiv-backend/domain/model/originacion"

type BancoRepository interface {
	FindAll(u []*model.BancoResponse, params model.BancoQuery) ([]*model.BancoResponse, error)
	FindOne(id string, u *model.BancoResponse) (*model.BancoResponse, error)
	Create(u *model.Banco) (*model.BancoResponse, error)
	Update(id string, u *model.Banco) (*model.BancoResponse, error)
	Delete(id string, u *model.Banco) (*model.Banco, error)
}
