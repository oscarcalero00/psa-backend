package public

import model "sifiv-backend/domain/model/public"

type SolicitudRepository interface {
	FindAll(u []*model.SolicitudResponse, params model.SolicitudQuery) ([]*model.SolicitudResponse, error)
	FindOne(id string, u *model.SolicitudResponse) (*model.SolicitudResponse, error)
	Create(u *model.Solicitud) (*model.SolicitudResponse, error)
	Update(id string, u *model.Solicitud) (*model.SolicitudResponse, error)
	Delete(id string, u *model.Solicitud) (*model.Solicitud, error)
}
