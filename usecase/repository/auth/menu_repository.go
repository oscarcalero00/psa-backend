package auth

import model "sifiv-backend/domain/model/auth"

type MenuRepository interface {
	Menu(mo *model.Menu) ([]*model.MenuReponse, error)	
}
