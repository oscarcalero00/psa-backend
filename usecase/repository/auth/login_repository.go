package auth

import model "sifiv-backend/domain/model/auth"

type LoginRepository interface {
	Login(mo *model.Login) (*model.LoginReponse, error)	
}
