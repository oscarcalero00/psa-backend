package originacion

import model "sifiv-backend/domain/model/public"

type SolicitudPresenter interface {
	ResponseSolicituds(u []*model.SolicitudResponse) []*model.SolicitudResponse
	ResponseSolicitud(u *model.SolicitudResponse) *model.SolicitudResponse
}
