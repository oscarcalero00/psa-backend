package auth

import model "sifiv-backend/domain/model/auth"

type MenuPresenter interface {
	ResponseMenu(re []model.MenuList) []model.MenuList
}
