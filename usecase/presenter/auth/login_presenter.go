package auth

import model "sifiv-backend/domain/model/auth"

type LoginPresenter interface {
	ResponseLogin(re *model.LoginReponse) *model.LoginReponse
}
