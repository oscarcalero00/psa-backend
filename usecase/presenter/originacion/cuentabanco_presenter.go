package originacion

import model "sifiv-backend/domain/model/originacion"

type CuentaBancoPresenter interface {
	ResponseCuentaBancos(u []*model.CuentaBancoResponse) []*model.CuentaBancoResponse
	ResponseCuentaBanco(u *model.CuentaBancoResponse) *model.CuentaBancoResponse
}
