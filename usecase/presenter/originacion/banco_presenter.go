package originacion

import model "sifiv-backend/domain/model/originacion"

type BancoPresenter interface {
	ResponseBancos(u []*model.BancoResponse) []*model.BancoResponse
	ResponseBanco(u *model.BancoResponse) *model.BancoResponse
}
