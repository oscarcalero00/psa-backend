package originacion

import (
	"errors"
	model "sifiv-backend/domain/model/public"
	presenter "sifiv-backend/usecase/presenter/public"
	repositorybase "sifiv-backend/usecase/repository"
	repository "sifiv-backend/usecase/repository/public"
)

type solicitudInteractor struct {
	SolicitudRepository repository.SolicitudRepository
	SolicitudPresenter  presenter.SolicitudPresenter
	DBRepository        repositorybase.DBRepository
}

type SolicitudInteractor interface {
	Get(u []*model.SolicitudResponse, params model.SolicitudQuery) ([]*model.SolicitudResponse, error)
	Create(u *model.Solicitud) (*model.SolicitudResponse, error)
	GetRecord(id string, u *model.SolicitudResponse) (*model.SolicitudResponse, error)
	Update(id string, u *model.Solicitud) (*model.SolicitudResponse, error)
	Delete(id string, u *model.Solicitud) (*model.Solicitud, error)
}

func NewSolicitudInteractor(r repository.SolicitudRepository, p presenter.SolicitudPresenter, d repositorybase.DBRepository) SolicitudInteractor {
	return &solicitudInteractor{r, p, d}
}

func (us *solicitudInteractor) Get(u []*model.SolicitudResponse, params model.SolicitudQuery) ([]*model.SolicitudResponse, error) {
	u, err := us.SolicitudRepository.FindAll(u, params)

	if err != nil {
		return nil, err
	}

	return us.SolicitudPresenter.ResponseSolicituds(u), nil
}

func (us *solicitudInteractor) Create(u *model.Solicitud) (*model.SolicitudResponse, error) {
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {

		u, err := us.SolicitudRepository.Create(u)
		// do mailing
		// do logging
		// do another process

		return u, err
	})
	solicitud, ok := data.(*model.SolicitudResponse)
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return solicitud, nil
}

func (us *solicitudInteractor) GetRecord(id string, u *model.SolicitudResponse) (*model.SolicitudResponse, error) {
	u, err := us.SolicitudRepository.FindOne(id, u)

	if err != nil {
		return nil, err
	}

	return us.SolicitudPresenter.ResponseSolicitud(u), nil
}

func (us *solicitudInteractor) Update(id string, u *model.Solicitud) (*model.SolicitudResponse, error) {
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {

		u, err := us.SolicitudRepository.Update(id, u)
		// do mailing
		// do logging
		// do another process

		return u, err
	})
	solicitud, ok := data.(*model.SolicitudResponse)
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return solicitud, nil
}

func (us *solicitudInteractor) Delete(id string, u *model.Solicitud) (*model.Solicitud, error) {
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {

		u, err := us.SolicitudRepository.Delete(id, u)
		// do mailing
		// do logging
		// do another process

		return u, err
	})
	solicitud, ok := data.(*model.Solicitud)
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return solicitud, nil
}
