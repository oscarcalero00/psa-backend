package auth

import (
	//"errors"

	model "sifiv-backend/domain/model/auth"
	presenter "sifiv-backend/usecase/presenter/auth"
	repositorybase "sifiv-backend/usecase/repository"
	repository "sifiv-backend/usecase/repository/auth"

	"github.com/dgrijalva/jwt-go"
	//"github.com/jinzhu/gorm"
)

type loginInteractor struct {
	LoginRepository repository.LoginRepository
	LoginPresenter  presenter.LoginPresenter
	DBRepository    repositorybase.DBRepository
}

type LoginInteractor interface {
	Login(mo *model.Login) (*model.LoginReponse, error)
}

func NewLoginInteractor(r repository.LoginRepository, p presenter.LoginPresenter, d repositorybase.DBRepository) LoginInteractor {
	return &loginInteractor{r, p, d}
}

func (us *loginInteractor) Login(m *model.Login) (*model.LoginReponse, error) {

	u, err := us.LoginRepository.Login(m)

	if err != nil {
		return nil, err
	}

	if u != nil {
		token := jwt.New(jwt.SigningMethodHS256)

		// Set claims
		claims := token.Claims.(jwt.MapClaims)
		claims["name"] = u.Nombrecompleto
		claims["admin"] = true
		//claims["exp"] = time.Now().Add(time.Hour * 72).Unix()

		claims["usuario_perfil"] = u.Cargo
		claims["usuario_codigo"] = u.Usuario_codigo

		// Generate encoded token and send it as response.
		t, err := token.SignedString([]byte("secret"))
		if err != nil {
			return nil, err
		}
		u.Token = t
	}

	return us.LoginPresenter.ResponseLogin(u), nil
}
