package auth

import (
	//"errors"
	"fmt"	
	"strings"
	model "sifiv-backend/domain/model/auth"
	presenter "sifiv-backend/usecase/presenter/auth"
	repositorybase "sifiv-backend/usecase/repository"
	repository "sifiv-backend/usecase/repository/auth"
	//"github.com/dgrijalva/jwt-go"
	//"time"
	//"github.com/jinzhu/gorm"
)


func Filter(arr []model.MenuList, cond func(int) bool) []model.MenuList {
	result := []model.MenuList{}
	for i := range arr {
	  if cond(arr[i].Nivel) {
		 result = append(result, arr[i])
	  }
	}
	return result
  }

  func FilterCodigoMenu(arr []model.MenuList, cond func(string) bool) []model.MenuList {
	result := []model.MenuList{}
	for i := range arr {
	  if cond(arr[i].Codigopadre) {
		 result = append(result, arr[i])
	  }
	}
	return result
  }


type menuInteractor struct {
	MenuRepository repository.MenuRepository
	MenuPresenter  presenter.MenuPresenter
	DBRepository    repositorybase.DBRepository
}

type MenuInteractor interface {
	Menu(mo *model.Menu) ([]model.MenuList, error)	
}

func NewMenuInteractor(r repository.MenuRepository, p presenter.MenuPresenter, d repositorybase.DBRepository) MenuInteractor {
	return &menuInteractor{r, p, d}
}

func (us *menuInteractor) Menu(m *model.Menu) ([]model.MenuList, error) {
	
	var result  []model.MenuList 

	u, err := us.MenuRepository.Menu(m)


	if err != nil {
		return nil, err
	}
	if u != nil {
		//var resp = model.LoginReponse{}
		var _menu  []  model.MenuList 
		var maxnivel int = 0
		var cantniveles int = 0
		//var menu = make(map[string][]menu_struct)
		for _, element := range u {
			cantniveles = strings.Count(element.Progmodu_codigomenu,".")
			var nivel = strings.LastIndex(element.Progmodu_codigomenu,".");
			var codigomenu = "";
			var icon  = "pi pi-fw pi-align-left"
			var _ruta  = ""
			//var badgeclase  = ""
			if nivel != -1 {				
				codigomenu = element.Progmodu_codigomenu[:nivel];
				//badgeclase  = "p-badge-warning"
			}else{
				//codigomenu = element.Progmodu_codigomenu;				
				codigomenu = "00" ;	
				icon = "pi pi-fw pi-star"			
			}

			if(maxnivel< cantniveles){
				maxnivel = cantniveles
			}

		

			if(element.Progmodu_tipo == "M" ) {
				if element.Progmodu_modulo == "ORG" {
					_ruta = "/originacion"
				}
				if element.Progmodu_modulo == "ADM" {
					_ruta = "/administracion"
				}
			}else{
				_ruta = element.Progmodu_url 
			}

			if(element.Progmodu_tipo == "M" ) {
				_menu = append(_menu,model.MenuList{ 
					Label        : element.Progmodu_descripcion,
					//Url          : element.Progmodu_url,
					//Url          : _ruta,
					Codigomenu   : element.Progmodu_codigomenu,
					Tipoopcion   : element.Progmodu_tipo,
					Codigopadre  : codigomenu,
					Icon         : icon ,
					//BadgeClass   : badgeclase,
					Nivel        : cantniveles ,
				})	
			}else{
				_menu = append(_menu,model.MenuList{ 
					Label        : element.Progmodu_descripcion,
					//Url          : element.Progmodu_url,
					Url          : _ruta,
					Codigomenu   : element.Progmodu_codigomenu,
					Tipoopcion   : element.Progmodu_tipo,
					Codigopadre  : codigomenu,
					Icon         : icon ,
					//BadgeClass   : badgeclase,
					Nivel        : cantniveles ,
				})	
			}	

			
		}

		result := Filter(_menu, func(val int) bool {
			return val == 0
		})

		for index, element := range result {
			result[index].Items =  FilterCodigoMenu(_menu, func(val string) bool {
				return val == element.Codigomenu
			}) 
		}	

		for _, element := range result {
			for index2, element2 := range element.Items {
				element.Items[index2].Items =  FilterCodigoMenu(_menu, func(val string) bool {
					return val == element2.Codigomenu
				}) 
			}
		}

		for _, element := range result {
			for _, element2 := range element.Items {
				for index3, element3 := range element2.Items {
					element2.Items[index3].Items =  FilterCodigoMenu(_menu, func(val string) bool {
						return val == element3.Codigomenu
					}) 
				}
			}
		}

		fmt.Println( result)
		return us.MenuPresenter.ResponseMenu(result), nil
	}

	

	return us.MenuPresenter.ResponseMenu(result), nil
}

