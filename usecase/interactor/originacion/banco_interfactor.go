package originacion

import (
	"errors"
	model "sifiv-backend/domain/model/originacion"
	presenter "sifiv-backend/usecase/presenter/originacion"
	repository "sifiv-backend/usecase/repository/originacion"
	repositorybase "sifiv-backend/usecase/repository" 
)

type bancoInteractor struct {
	BancoRepository   repository.BancoRepository
	BancoPresenter    presenter.BancoPresenter
	DBRepository     repositorybase.DBRepository
}

type BancoInteractor interface {
	Get(u []*model.BancoResponse,params model.BancoQuery) ([]*model.BancoResponse, error)
	Create(u *model.Banco) (*model.BancoResponse, error)	
	GetRecord(id string,u *model.BancoResponse) (*model.BancoResponse, error)
	Update(id string,u *model.Banco) (*model.BancoResponse, error)
	Delete(id string,u *model.Banco) (*model.Banco, error)
}

func NewBancoInteractor(r repository.BancoRepository, p presenter.BancoPresenter, d repositorybase.DBRepository) BancoInteractor {
	return &bancoInteractor{r, p, d}
}

func (us *bancoInteractor) Get(u []*model.BancoResponse,params model.BancoQuery) ([]*model.BancoResponse, error) {
	u, err := us.BancoRepository.FindAll(u,params)	

	if err != nil {
		return nil, err
	}

	return us.BancoPresenter.ResponseBancos(u), nil
}

func (us *bancoInteractor) Create(u *model.Banco) (*model.BancoResponse, error) {	
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {
		
		u, err := us.BancoRepository.Create(u)		
		// do mailing
		// do logging
		// do another process
		
		return u, err
	})		
	banco, ok := data.(*model.BancoResponse)	
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return banco, nil
}

func (us *bancoInteractor) GetRecord(id  string,u *model.BancoResponse) (*model.BancoResponse, error) {
	u, err := us.BancoRepository.FindOne(id,u)	

	if err != nil {
		return nil, err
	}

	return us.BancoPresenter.ResponseBanco(u), nil
}



func (us *bancoInteractor) Update(id  string,u *model.Banco) (*model.BancoResponse, error) {	
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {
		
		u, err := us.BancoRepository.Update(id,u)		
		// do mailing
		// do logging
		// do another process
		
		return u, err
	})		
	banco, ok := data.(*model.BancoResponse)	
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return banco, nil
}

func (us *bancoInteractor) Delete(id  string,u *model.Banco) (*model.Banco, error) {	
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {
		
		u, err := us.BancoRepository.Delete(id,u)		
		// do mailing
		// do logging
		// do another process
		
		return u, err
	})		
	banco, ok := data.(*model.Banco)	
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return banco, nil
}