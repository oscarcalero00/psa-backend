package originacion

import (
	"errors"
	model "sifiv-backend/domain/model/originacion"
	presenter "sifiv-backend/usecase/presenter/originacion"
	repository "sifiv-backend/usecase/repository/originacion"
	repositorybase "sifiv-backend/usecase/repository" 
)

type cuentaBancoInteractor struct {
	CuentaBancoRepository   repository.CuentaBancoRepository
	CuentaBancoPresenter    presenter.CuentaBancoPresenter
	DBRepository     repositorybase.DBRepository
}

type CuentaBancoInteractor interface {
	Get(u []*model.CuentaBancoResponse,params model.CuentaBancoQuery) ([]*model.CuentaBancoResponse, error)
	Create(u *model.CuentaBanco) (*model.CuentaBancoResponse, error)	
	GetRecord(id string,u *model.CuentaBancoResponse) (*model.CuentaBancoResponse, error)
	Update(id string,u *model.CuentaBanco) (*model.CuentaBancoResponse, error)
	Delete(id string,u *model.CuentaBanco) (*model.CuentaBanco, error)
}

func NewCuentaBancoInteractor(r repository.CuentaBancoRepository, p presenter.CuentaBancoPresenter, d repositorybase.DBRepository) CuentaBancoInteractor {
	return &cuentaBancoInteractor{r, p, d}
}

func (us *cuentaBancoInteractor) Get(u []*model.CuentaBancoResponse,params model.CuentaBancoQuery) ([]*model.CuentaBancoResponse, error) {
	u, err := us.CuentaBancoRepository.FindAll(u,params)	

	if err != nil {
		return nil, err
	}

	return us.CuentaBancoPresenter.ResponseCuentaBancos(u), nil
}

func (us *cuentaBancoInteractor) Create(u *model.CuentaBanco) (*model.CuentaBancoResponse, error) {	
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {
		
		u, err := us.CuentaBancoRepository.Create(u)		
		// do mailing
		// do logging
		// do another process
		
		return u, err
	})		
	cuentaBanco, ok := data.(*model.CuentaBancoResponse)	
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return cuentaBanco, nil
}

func (us *cuentaBancoInteractor) GetRecord(id  string,u *model.CuentaBancoResponse) (*model.CuentaBancoResponse, error) {
	u, err := us.CuentaBancoRepository.FindOne(id,u)	

	if err != nil {
		return nil, err
	}

	return us.CuentaBancoPresenter.ResponseCuentaBanco(u), nil
}



func (us *cuentaBancoInteractor) Update(id  string,u *model.CuentaBanco) (*model.CuentaBancoResponse, error) {	
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {
		
		u, err := us.CuentaBancoRepository.Update(id,u)		
		// do mailing
		// do logging
		// do another process
		
		return u, err
	})		
	cuentaBanco, ok := data.(*model.CuentaBancoResponse)	
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return cuentaBanco, nil
}

func (us *cuentaBancoInteractor) Delete(id  string,u *model.CuentaBanco) (*model.CuentaBanco, error) {	
	data, err := us.DBRepository.Transaction(func(i interface{}) (interface{}, error) {
		
		u, err := us.CuentaBancoRepository.Delete(id,u)		
		// do mailing
		// do logging
		// do another process
		
		return u, err
	})		
	cuentaBanco, ok := data.(*model.CuentaBanco)	
	if !ok {
		return nil, errors.New("cast error")
	}

	if !errors.Is(err, nil) {
		return nil, err
	}

	return cuentaBanco, nil
}