package config

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/viper"
)

type Database struct {
	User                 string
	Password             string
	Net                  string
	Addr                 string
	Port                 string
	DBName               string
	AllowNativePasswords bool
	Params               struct {
		ParseTime string
	}
}

type Config struct {
	Database struct {
		Local      Database
		Develoment Database
		Test       Database
	}
	Server struct {
		Address string
	}
	SERVERENV string
}

var C Config

func ReadConfig() {
	Config := &C

	viper.SetConfigName("config")
	viper.SetConfigType("yml")
	//viper.AddConfigPath(filepath.Join("$GOPATH", "src", "github.com", "manakuro", "sifiv-backend", "config"))
	viper.AddConfigPath(filepath.Join("./config"))
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
		log.Fatalln(err)
	}

	if err := viper.Unmarshal(&Config); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	//spew.Dump(C)
}
