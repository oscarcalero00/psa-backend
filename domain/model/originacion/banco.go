package originacion

type Banco struct {
	Banco_codigo               string `gorm:"primary_key" json:"banco_codigo"`
	Banco_numeroidentificacion string `json:"banco_numeroidentificacion"`
	Banco_codigodireccion      string `json:"banco_codigodireccion"`
	Banco_codigocompensacion   string `json:"banco_codigocompensacion"`
	Banco_fregistro            string `json:"banco_fregistro"`
	Banco_estado               string `json:"banco_estado"`
	Banco_festado              string `json:"banco_festado"`
	Banco_apellido1            string `json:"banco_apellido1"`
	Banco_apellido2            string `json:"banco_apellido2"`
	Banco_nombre1              string `json:"banco_nombre1"`
	Banco_nombre2              string `json:"banco_nombre2"`
	Banco_nombrecompleto       string `json:"banco_nombrecompleto"`
	Banco_tipopersona          string `json:"banco_tipopersona"`
	Banco_departamento         string `json:"banco_departamento"`
	Banco_ciudad               string `json:"banco_ciudad"`
	Banco_direccion            string `json:"banco_direccion"`
	Banco_telefono             string `json:"banco_telefono"`
	Banco_email                string `json:"banco_email"`
	Banco_celular              string `json:"banco_celular"`
	Banco_tipoidentificacion   string `json:"banco_tipoidentificacion"`
}

func (Banco) TableName() string { return "public.tmp_banco_operacion" }

type BancoResponse struct {
	Banco_codigo               string `gorm:"primary_key" json:"banco_codigo"`
	Banco_numeroidentificacion string `json:"banco_numeroidentificacion"`
	Banco_codigodireccion      string `json:"banco_codigodireccion"`
	Banco_codigocompensacion   string `json:"banco_codigocompensacion"`
	Banco_fregistro            string `json:"banco_fregistro"`
	Banco_estado               string `json:"banco_estado"`
	Banco_festado              string `json:"banco_festado"`

	Banco_apellido1          string `json:"banco_apellido1"`
	Banco_apellido2          string `json:"banco_apellido2"`
	Banco_nombre1            string `json:"banco_nombre1"`
	Banco_nombre2            string `json:"banco_nombre2"`
	Banco_nombrecompleto     string `json:"banco_nombrecompleto"`
	Banco_tipopersona        string `json:"banco_tipopersona"`
	Banco_departamento       string `json:"banco_departamento"`
	Banco_ciudad             string `json:"banco_ciudad"`
	Banco_direccion          string `json:"banco_direccion"`
	Banco_telefono           string `json:"banco_telefono"`
	Banco_email              string `json:"banco_email"`
	Banco_celular            string `json:"banco_celular"`
	Banco_tipoidentificacion string `json:"banco_tipoidentificacion"`
	Banco_ciudadnombre       string `json:"banco_ciudadnombre"`
}

func (BancoResponse) TableName() string { return "public.banco" }

type BancoQuery struct {
	Banco_codigo               string `json:"banco_codigo"`
	Banco_numeroidentificacion string `json:"banco_numeroidentificacion"`
	Banco_nombrecompleto       string `json:"banco_nombrecompleto"`
}
