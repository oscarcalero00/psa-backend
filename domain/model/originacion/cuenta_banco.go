package originacion



type CuentaBanco struct {	
	
	Cuenbanc_id       			string  `gorm:"primary_key" json:"cuenbanc_id"`
	Cuenbanc_banco  			string  `json:"cuenbanc_banco"`
	Cuenbanc_numerocuenta      	string  `json:"cuenbanc_numerocuenta"`	
	Cuenbanc_empresa       		string  `json:"cuenbanc_empresa"`	
	Cuenbanc_tipoproductobanco  string  `json:"cuenbanc_tipoproductobanco"`	
	Cuenbanc_excentagmf       	string  `json:"cuenbanc_excentagmf"`	
	Cuenbanc_tienecupocrdito    string  `json:"cuenbanc_tienecupocrdito"`	

	Cuenbanc_valorcupo  		string  `json:"cuenbanc_valorcupo"`
	Cuenbanc_fvigencia  		string  `json:"cuenbanc_fvigencia"`
	Cuenbanc_diacortepago  		string     	`json:"cuenbanc_diacortepago"`
	Cuenbanc_tipotasa  			string  `json:"cuenbanc_tipotasa"`
	Cuenbanc_tasabase  			string  `json:"cuenbanc_tasabase"`
	Cuenbanc_puntosfijosea  	string  `json:"cuenbanc_puntosfijosea"`
	Cuenbanc_fregistro  		string  `json:"cuenbanc_fregistro"`
	Cuenbanc_estado  			string  `json:"cuenbanc_estado"`
	Cuenbanc_festado  			string  `json:"cuenbanc_festado"`
	
		
}

func (CuentaBanco) TableName() string { return "public.cuenta_banco" }

type CuentaBancoResponse struct {	
	Cuenbanc_id       			string  `gorm:"primary_key" json:"cuenbanc_id"`
	Cuenbanc_banco  			string  `json:"cuenbanc_banco"`
	Cuenbanc_numerocuenta      	string  `json:"cuenbanc_numerocuenta"`	
	Cuenbanc_empresa       		string  `json:"cuenbanc_empresa"`	
	Cuenbanc_tipoproductobanco  string  `json:"cuenbanc_tipoproductobanco"`	
	Cuenbanc_excentagmf       	string  `json:"cuenbanc_excentagmf"`	
	Cuenbanc_tienecupocrdito    string  `json:"cuenbanc_tienecupocrdito"`	

	Cuenbanc_valorcupo  		string  `json:"cuenbanc_valorcupo"`
	Cuenbanc_fvigencia  		string  `json:"cuenbanc_fvigencia"`
	Cuenbanc_diacortepago  		string  `json:"cuenbanc_diacortepago"`
	Cuenbanc_tipotasa  			string  `json:"cuenbanc_tipotasa"`
	Cuenbanc_tasabase  			string  `json:"cuenbanc_tasabase"`
	Cuenbanc_puntosfijosea  	string  `json:"cuenbanc_puntosfijosea"`
	Cuenbanc_fregistro  		string  `json:"cuenbanc_fregistro"`
	Cuenbanc_estado  			string  `json:"cuenbanc_estado"`
	Cuenbanc_festado  			string  `json:"cuenbanc_festado"`	
	Empresa_nombre  			string  `json:"empresa_nombre"`	
	Tasabase_nombre  			string  `json:"tasabase_nombre"`	
	Tipotasa_nombre  			string  `json:"tipotasa_nombre"`	
	Tipoprba_nombre  			string  `json:"tipoprba_nombre"`	

	
	
}
func (CuentaBancoResponse) TableName() string { return "public.cuenta_banco" }

type CuentaBancoQuery struct {	
	Cuenbanc_banco   					string  `json:"cuenbanc_banco"`		
}
