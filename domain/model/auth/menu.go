package auth

//import "time"

type Menu struct {

}

type MenuList struct {
    Label        string `json:"label"`
    Url          string `json:"routerLink"`
	Codigomenu   string `json:"codigomenu"`
	Codigopadre  string `json:"codigopadre"`
	Tipoopcion   string `json:"tipomenu"`
	Nivel        int    `json:"nivel"`
	Icon		 string `json:"icon"`
	//BadgeClass   string `json:"badgeClass"`
	//Badge        string `json:"badge"`
	Items        []MenuList `json:"items"`	
}

//func (Menu) TableName() string { return "auth.users" }

type MenuReponse struct {		
	
	ID        				string     `gorm:"primary_key" json:"progmodu_id"`	
	Progmodu_modulo    		string     `gorm:"column:progmodu_modulo" json:"progmodu_modulo"`
	Progmodu_programa      	string     `gorm:"column:progmodu_programa" json:"progmodu_programa"`
	Progmodu_descripcion    string     `gorm:"column:progmodu_descripcion" json:"progmodu_descripcion"`
	Progmodu_tipo      		string     `gorm:"column:progmodu_tipo" json:"progmodu_tipo"`
	Progmodu_codigomenu 	string     `gorm:"column:progmodu_codigomenu" json:"progmodu_codigomenu"`	
	Progmodu_url      		string     `gorm:"column:progmodu_url" json:"progmodu_url"`
	Progmodu_estado   		string     `gorm:"column:progmodu_estado" json:"progmodu_estado"`
	
}
func (MenuReponse) TableName() string { return "public.programa_modulo" }

