package auth

//import "time"

type Login struct {
	//Id        	uint       `gorm:"primary_key" json:"id"`
	Email    string `json:"email"`
	Password string `json:"password"`
	//CreatedAt *time.Time `json:"created_at"`
	//UpdatedAt *time.Time `json:"updated_at"`
	//DeletedAt *time.Time `json:"deleted_at"`
}

//func (Login) TableName() string { return "auth.users" }

type LoginReponse struct {
	/*
		Id        		uint       	`gorm:"primary_key" json:"id"`
		Nombre1      	string     `gorm:"column:users_nombre1" json:"users_nombre1"`
		Nombre2      	string     `gorm:"column:users_nombre2" json:"users_nombre2"`
		Apellido1      	string     `gorm:"column:users_apellido1" json:"users_apellido1"`
		Apellido2      	string     `gorm:"column:users_apellido2" json:"users_apellido2"`
		Nombrecompleto 	string     `gorm:"column:users_nombrecompleto" json:"users_nombrecompleto"`
		Email      		string     `gorm:"column:users_email" json:"users_email"`
		Password   		string     `gorm:"column:users_password" json:"users_password"`
		Rol   			string     `gorm:"column:users_userrol" json:"users_rol"`
		Token   		string     `json:"token"`
	*/
	Usuario_codigo       string `gorm:"primary_key" json:"usuario_codigo"`
	NumeroIdentificacion string `gorm:"column:usuario_numeroidentificacion" json:"usuario_numeroidentificacion"`
	CodigoDireccion      string `gorm:"column:usuario_codigodireccion" json:"usuario_codigodireccion"`
	Perfil               string `gorm:"column:usuario_perfil" json:"usuario_perfil"`
	Cargo                string `gorm:"column:usuario_cargo" json:"usuario_cargo"`
	Nombrecompleto       string `gorm:"column:tercdire_nombrecomercial" json:"tercdire_nombrecomercial"`
	Email                string `gorm:"column:tercdire_email" json:"tercdire_email"`
	Telefono             string `gorm:"column:tercdire_telefono" json:"tercdire_telefono"`
	Celular              string `gorm:"column:tercdire_celular" json:"tercdire_celular"`
	Departamento         string `gorm:"column:tercdire_departamento" json:"tercdire_departamento"`
	Ciudad               string `gorm:"column:tercdire_ciudad" json:"tercdire_ciudad"`
	Direccion            string `gorm:"column:tercdire_direccion" json:"tercdire_direccion"`
	Token                string `json:"token"`
}

func (LoginReponse) TableName() string { return "public.usuario" }
