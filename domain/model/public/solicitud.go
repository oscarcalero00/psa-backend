package public

type Solicitud struct {
	Id               string `gorm:"primary_key" json:"id"`
	Solicitud_nombre string `json:"solicitud_nombre"`
	Solicitud_cedula string `json:"solicitud_cedula"`
}

func (Solicitud) TableName() string { return "public.solicitudes" }

type SolicitudResponse struct {
	Id               string `gorm:"primary_key" json:"id"`
	Solicitud_nombre string `json:"solicitud_nombre"`
	Solicitud_cedula string `json:"solicitud_cedula"`
}

func (SolicitudResponse) TableName() string { return "public.solicitudes" }

type SolicitudQuery struct {
	Id               string `gorm:"primary_key" json:"id"`
	Solicitud_nombre string `json:"solicitud_nombre"`
	Solicitud_cedula string `json:"solicitud_cedula"`
}
