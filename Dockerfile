## We specify the base image we need for our
## go application
FROM golang
#RUN apk add git
## We create an /app directory within our
## image that will hold our application source
## files
RUN mkdir /app
## We copy everything in the root directory
## into our /app directory
ADD . /app
## We specify that we now wish to execute
## any further commands inside our /app
## directory
WORKDIR /app
## we run go build to compile the binary
## executable of our Go program
RUN go get -d -v ./...
RUN go build -o main .
RUN go install -v ./...
## Our start command which kicks off
## our newly created binary executable
#EXPOSE 8081

CMD ["/app/main"]
#CMD go run .
#CMD ["go run /app/main"]